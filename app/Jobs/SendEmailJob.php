<?php

namespace App\Jobs;

use App\Exports\SupporterExport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $email = '';
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email =  $email;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $name = md5(date('Y-m-d H:i:s')).'.xlsx';
        $path ='public/'.$name;


        $return = Excel::store(new SupporterExport(), $path);
        if($return) {
            try {
                Mail::raw('Oi, Finalizamos a exportação dos seus dados segue ai o link para download. '.url("/storage/{$name}"), function ($message) {               //
                    $message->from('comercial@luansimoes.com.br', '');

                    $message->to($this->email);

                });

            } catch ( \Exception $e) {
                echo $e->getMessage();
            }


        }
    }
}
