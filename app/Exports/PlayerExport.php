<?php

namespace App\Exports;

use App\Player;
use App\Team;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PlayerExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Player::with(['city.state'])->get();
    }
    /**
     * @var Team $invoice
     */
    public function map($player): array
    {
        return [
            $player->id,
            $player->name,
            $player->height,
            $player->weight,
            $player->city->name.'/'.$player->city->state->abbr,
            $player->team->name,
            date('d/m/Y', strtotime($player->date_of_birth)),
            date('d/m/Y', strtotime($player->created_at)),
            date('d/m/Y', strtotime($player->updated_at)),
        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'Nome',
            'Altura',
            'Peso',
            'Cidade/Estado',
            'Time',
            'Data de aniversario',
            'Data de Criação',
            'Ultima atualização',
        ];
    }
}
