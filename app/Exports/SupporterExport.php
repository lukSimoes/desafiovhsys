<?php

namespace App\Exports;

use App\Player;
use App\Supporter;
use App\Team;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SupporterExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Supporter::with(['city.state','team'])->get();
    }
    /**
     * @var Team $invoice
     */
    public function map($supporter): array
    {
        return [
            $supporter->id,
            $supporter->name,
            $supporter->cpf,
            $supporter->city->name.'/'.$supporter->city->state->abbr,
            $supporter->team->name,
            date('d/m/Y', strtotime($supporter->date_of_birth)),
            date('d/m/Y', strtotime($supporter->created_at)),
            date('d/m/Y', strtotime($supporter->updated_at)),
        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'Nome',
            'CPF',
            'Cidade/Estado',
            'Time',
            'Data de aniversario',
            'Data de Criação',
            'Ultima atualização',
        ];
    }
}
