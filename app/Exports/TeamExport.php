<?php

namespace App\Exports;

use App\Team;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TeamExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Team::with(['city.state'])->get();
    }
    /**
     * @var Team $invoice
     */
    public function map($team): array
    {
        return [
            $team->id,
            $team->name,
            $team->city->name.'/'.$team->city->state->abbr,
            date('d/m/Y', strtotime($team->founded_in)),
            $team->mascot,
            date('d/m/Y', strtotime($team->created_at)),
            date('d/m/Y', strtotime($team->updated_at)),
        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'Nome',
            'Cidade/Estado',
            'Data de fundação',
            'Mascote',
            'Data de Criação',
            'Ultima atualização',
        ];
    }
}
