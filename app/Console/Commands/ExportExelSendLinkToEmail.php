<?php

namespace App\Console\Commands;

use App\Exports\PlayerExport;
use App\Exports\SupporterExport;
use Illuminate\Console\Command;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ExportExelSendLinkToEmail extends Command implements  ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:exportExelSendLinkToEamil';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria o exel e envia por email';
    protected $email = '';

    /**
     * Create a new command instance.
     *
     * @param string $email
     */
    public function __construct()
    {

        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $name = md5(date('Y-m-d H:i:s')).'.xlsx';
        $path ='public/'.$name;


       $return = Excel::store(new SupporterExport(), $path);
       if($return) {
           Mail::raw('Oi, Finalssssizamos asaa exportação dos seus dados segue ai o link para download. '.url("/storage/{$name}"), function ($message) {               //
               $message->from('comercial@luansimoes.com.br', 'Laravel');

               $message->to(Auth::user()->email);

           });
       }
    }
}
