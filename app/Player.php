<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    //
    protected $table = 'players';
    protected $fillable = ['name', 'height', 'weight', 'id_city', 'date_of_birth','id_team'];

    public function city()
    {

        return $this->belongsTo(City::class, 'id_city');

    }

    public function team()
    {

        return $this->belongsTo(Team::class, 'id_team');

    }

}
