<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //
    protected $table = 'teams';
    protected $fillable = ['name', 'id_city', 'founded_in', 'mascot'];

    public function city()
    {

        return $this->belongsTo(City::class, 'id_city');

    }

    public function players()
    {

        return $this->hasMany(Player::class,'id_team');

    }


}
