<?php

namespace App\Http\Controllers;

use App\City;
use App\Exports\TeamExport;
use App\Player;
use App\State;
use App\Team;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TeamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $teams = Team::orderBy('created_at', 'desc')->with(['city.state'])->get();

        return view('team.home', ['teams'=>$teams]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')){
            $states = State::orderBy('name','asc')->get();
            return view('team.form',['states'=>$states]);
        }
        else {
            $rules = [
                'name' => 'required',
            ];
            $this->validate($request, $rules);

            $data = $request->all();
            $team = new Team();
            $team->fill($data);
            $data = str_replace("/", "-", $team->founded_in);
            $team->founded_in =  date('Y-m-d', strtotime($data));
            $team->save();
            return redirect('/times');
        }
    }

    public function delete($id)
    {
        Team::destroy($id);
        Player::where(['id_team'=>$id])->update(['id_team' => NULL]);
        return redirect('/times');
    }

    public function edit(Request $request, $id)
    {

            $team = Team::with(['city.state','players'])->where(['id'=>$id])->first();
            $team->founded_in =  date('Y-m-d', strtotime($team->founded_in));
            $states = State::orderBy('name','asc')->get();
            $players = Player::orderBy('name','asc')->where(['id_team'=>null])->get();
            $cities = City::where(['state_id'=>$team->city->state_id])->get();
            return view('team.form', ['team' => $team, 'states'=>$states, 'cities'=>$cities, 'players'=>$players]);

    }
    public function update(Request $request, $id)
    {

            $rules = [
                'name' => 'required',

            ];
            $this->validate($request, $rules);
            $data = $request->all();
            $team = Team::find($id);
            $team->fill($data);
            $data = str_replace("/", "-", $team->founded_in);
            $team->founded_in =  date('Y-m-d', strtotime($data));
            $team->save();
            return redirect('/times');

    }


    public function ajaxRequest(Request $request, $id)
    {
        $cities = City::where(['state_id'=>$id])->get();
        $retorno = '';
        foreach ($cities as $city) {
            $retorno .='<option value="'.$city->id.'">'.$city->name.'</option>';
        }
        echo $retorno;
    }

    public function ajaxRequestAddPlayer(Request $request)
    {
        $data = $request->all();
        $player = Player::find($data['idPlayer']);
        $player->id_team = $data['idTeam'];
        $player->save();
        $players = Player::orderBy('name','asc')->where(['id_team'=>null])->get();
        $retorno['playerTable'] = '';
        $retorno['player'] = '<option value="">Selecione um jogador</option>';
        foreach ($players as $player) {
            $retorno['player'] .='<option value="'.$player->id.'">'.$player->name.'</option>';
        }
        $team = Team::with(['city.state','players'])->where(['id'=>$data['idTeam']])->first();
        foreach ($team->players as $key =>$player) {
            $retorno['playerTable'] .='<tr>
                                    <td scope="col">'.($key+1).'</td>
                                    <td scope="col">'.$player->name.'</td>
                                    <td scope="col"><button type="button" data-iddelete="'.$player->id.'" class="btn btn-primary btn-sm btn_exclude"><i class="fa fa-trash"></i></button></td>
                                </tr>';
        }
        echo json_encode($retorno);



    }

    public function ajaxRequestRemovePlayer(Request $request)
    {
        $data = $request->all();
        $player = Player::find($data['idPlayer']);
        $player->id_team = NULL;
        $player->save();
        $players = Player::orderBy('name','asc')->where(['id_team'=>null])->get();
        $retorno['playerTable'] = '';
        $retorno['player'] = '<option value="">Selecione um jogador</option>';
        foreach ($players as $player) {
            $retorno['player'] .='<option value="'.$player->id.'">'.$player->name.'</option>';
        }
        $team = Team::with(['city.state','players'])->where(['id'=>$data['idTeam']])->first();
        foreach ($team->players as $key =>$player) {
            $retorno['playerTable'] .='<tr>
                                    <td scope="col">'.($key+1).'</td>
                                    <td scope="col">'.$player->name.'</td>
                                    <td scope="col"><button type="button" data-iddelete="'.$player->id.'" class="btn btn-primary btn-sm btn_exclude"><i class="fa fa-trash"></i></button></td>
                                </tr>';
        }
        echo json_encode($retorno);



    }

    public function downloadExcel() {
        return Excel::download(new TeamExport, 'teams.xlsx');
    }


}
