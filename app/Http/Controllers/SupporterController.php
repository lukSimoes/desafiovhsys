<?php

namespace App\Http\Controllers;

use App\City;
use App\Exports\PlayerExport;
use App\Jobs\SendEmailJob;
use App\Player;
use App\State;
use App\Supporter;
use App\Team;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SupporterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $models = Supporter::orderBy('id', 'asc')->with(['city.state','team'])->paginate(100);


        return view('supporters.home', ['models'=>$models]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')){
            $states = State::orderBy('name','asc')->get();
            $teams = Team::orderBy('name','asc')->get();
            return view('supporters.form',['states'=>$states, 'teams'=>$teams]);
        }
        else {
            $rules = [
                'name' => 'required',
            ];
            $this->validate($request, $rules);

            $data = $request->all();
            $model = new Supporter();
            $model->fill($data);
            $data = str_replace("/", "-", $model->date_of_birth);
            $model->date_of_birth =  date('Y-m-d', strtotime($data));
            $model->save();
            return redirect('/socios');
        }
    }

    public function delete($id)
    {
        Player::destroy($id);
        return redirect('/socios');
    }

    public function edit(Request $request, $id)
    {

        $model = Supporter::with(['city.state','team'])->where(['id'=>$id])->first();
        $teams = Team::orderBy('name','asc')->get();
        $model->date_of_birth =  date('Y-m-d', strtotime($model->date_of_birth));

        $states = State::orderBy('name','asc')->get();
        $cities = City::where(['state_id'=>$model->city->state_id])->get();
        return view('supporters.form', ['model' => $model,'teams'=>$teams, 'states'=>$states, 'cities'=>$cities]);

    }
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required',

        ];
        $this->validate($request, $rules);
        $data = $request->all();
        $model = Supporter::find($id);
        $model->fill($data);
        $data = str_replace("/", "-", $model->date_of_birth);
        $model->date_of_birth =  date('Y-m-d', strtotime($data));
        $model->save();
        return redirect('/socios');

    }


    public function ajaxRequest(Request $request, $id)
    {
        $cities = City::where(['state_id'=>$id])->get();
        $retorno = '';
        foreach ($cities as $city) {
            $retorno .='<option value="'.$city->id.'">'.$city->name.'</option>';
        }
        echo $retorno;
    }

    public function downloadExcel() {

        return Excel::download(new PlayerExport(), 'players.xlsx');
    }

    public function exportCommand($email) {

        $this->dispatch(new SendEmailJob($email));

       echo 'true';
    }
}
