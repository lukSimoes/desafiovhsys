@extends('layouts.default')
@section('content')


    <div class="container">
        <div class="float-right">
            <a href="{{route('player.export')}}" class="btn btn-primary">Export CSV</a>

            <a href="{{route('player.new')}}" class="btn btn-primary">Novo</a>
        </div>
        <h1 style="font-size: 2.2rem">Lista de times</h1>
        <hr/>
        <table class="table table-bordered bg-light">
            <thead class="bg-dark" style="color: white">
            <tr>
                <th width="60px" style="text-align: center">No</th>
                <th>Name</th>
                <th>Data de Aniversario</th>
                <th>Cidade/Estado</th>
                <th>Altura</th>
                <th>Peso</th>
                <th>Time</th>

                <th width="150px">Action</th>
            </tr>
            </thead>
            <tbody>
            @php
                $i=1;
            @endphp
            @foreach($models  as $key => $model)
                <tr>
                    <th style="text-align: center">{{$key + 1}}</th>
                    <td>{{$model->name}}</td>
                    <td>{{date('d/m/Y', strtotime($model->date_of_birth))}}</td>
                    <td>{{$model->city->name .'/'.$model->city->state->abbr}}</td>
                    <td>{{$model->height}}</td>
                    <td>{{$model->weight}}</td>
                    <td>{{isset($model->team) ? $model->team->name : ''}}</td>

                    <td align="center">
                        <a class="btn btn-primary btn-sm" title="Edit"
                           href="{{route('player.editar',$model->id)}}">
                            Edit</a>
                        <a class="btn btn-danger btn-sm" title="Delete"
                           href="{{route('player.deletar',$model->id)}}">
                            Delete
                        </a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <nav>
            <ul class="pagination justify-content-end">

            </ul>
        </nav>
    </div>
@endsection