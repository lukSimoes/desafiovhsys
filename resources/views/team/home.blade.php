@extends('layouts.default')
@section('content')


    <div class="container">
        <div class="float-right">
            <a href="{{route('form.export')}}" class="btn btn-primary">Export CSV</a>
            <a href="{{route('form.new')}}" class="btn btn-primary">Novo</a>
        </div>
        <h1 style="font-size: 2.2rem">Lista de times</h1>
        <hr/>
        <table class="table table-bordered bg-light">
            <thead class="bg-dark" style="color: white">
            <tr>
                <th width="60px" style="text-align: center">No</th>
                <th>Name</th>
                <th>Cidade/Estado</th>
                <th>Mascote</th>
                <th>Data de fundação</th>
                <th width="150px">Action</th>
            </tr>
            </thead>
            <tbody>
            @php
                $i=1;
            @endphp
            @foreach($teams  as $key => $team)
                <tr>
                    <th style="text-align: center">{{$key + 1}}</th>
                    <td>{{$team->name}}</td>
                    <td>{{$team->city->name .'/'.$team->city->state->abbr}}</td>
                    <td>{{$team->mascot}}</td>
                    <td>{{date('d/m/Y', strtotime($team->founded_in))}}</td>
                    <td align="center">
                        <a class="btn btn-primary btn-sm" title="Edit"
                           href="{{route('form.editar',$team->id)}}">
                            Edit</a>
                        <a class="btn btn-danger btn-sm" title="Delete"
                           href="{{route('form.deletar',$team->id)}}">
                            Delete
                        </a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <nav>
            <ul class="pagination justify-content-end">

            </ul>
        </nav>
    </div>
@endsection