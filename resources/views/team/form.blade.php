@extends('layouts.default')
@section('css')
    <style>
        .form-group.required label:after {
            content: " *";
            color: red;
            font-weight: bold;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
            <div class="card uper">
                <div class="card-header">
                    Adicionar Time
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form method="post" action="{{isset($team) ? route('form.atualizar',$team->id) : route('form.create')}}">
                        <div class="form-group">
                            @csrf
                            <label for="name">Nome</label>
                            <input type="text" class="form-control" name="name" value="{{isset($team) ? $team->name : ''}}"/>
                        </div>
                        <div class="form-group">
                            <label for="name">Data de nascimento</label>
                            <input type="date" class="form-control" name="founded_in" value="{{isset($team ) ? $team->founded_in : ''}}"/>

                        </div>
                        <div class="form-group">
                            <label for="price">Estado:</label>
                            <select  class="form-control " name="id_estado" id="id_estado">
                                <option value="">Selecione</option>
                                @foreach($states  as $state)
                                     <option {{ isset($team) && $team->city->state_id == $state->id ? 'selected':''}} value="{{$state->id}}">{{$state->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price">Cidade:</label>
                            <select  class="form-control " name="id_city" id="id_city">
                                <option value=""></option>
                                @if(isset($cities))
                                    @foreach($cities  as $city)
                                        <option {{isset($team) && $team->id_city == $city->id ? 'selected':''}} value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach
                               @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Mascote</label>
                            <input type="text" class="form-control" name="mascot" value="{{isset($team) ? $team->mascot : ''}}" />
                        </div>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </form>
                        @if(isset($players))

                            <div style="margin-top: 20px">
                          <h2>Jogadores</h2>
                            <form class="form-inline">

                                <div class="form-group  mb-2" style="margin-right: 10px">
                                    <select  class="form-control " name="id_jogador" id="id_jogador">
                                        <option value="">Selecione um jogador</option>
                                        @if(isset($players))
                                            @foreach($players  as $player)
                                                <option {{isset($team) && $team->id_city == $player->id ? 'selected':''}} value="{{$player->id}}">{{$player->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <button type="button" data-idteam="{{$team->id}}" class="btn btn-primary mb-2" id="add_plpayer">Adicionar</button>
                            </form>
                            <table class="table" style="text-align: center">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Opções</th>
                                </tr>
                                </thead>
                                <tbody class="tb_body">

                                    @if(isset($team->players))
                                        @foreach($team->players as $key =>$player )
                                            <tr>
                                            <td scope="row">{{$key +1}}</td>
                                            <td>{{$player['name']}}</td>
                                            <td><button type="button" data-iddelete="{{$player->id}}" class="btn btn-primary btn-sm btn_exclude"><i class="fa fa-trash"></i></button></td>
                                            </tr>
                                        @endforeach
                                    @endif



                                </tbody>
                            </table>

                        </div>


                        @endif
                </div>
            </div>
        </div>
    </div>

@endsection