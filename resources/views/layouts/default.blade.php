<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
@yield('head')
<!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    @yield('css')
</head>
<body style="padding-bottom: 50px;background: whitesmoke">
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ route('form.index') }}">
               TESTE VHsys
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link"href="{{ route('form.index') }}">Times</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('player.index') }}">Jogadores</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('support.index') }}">Socios</a>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                        @else
                            <li class="nav-item dropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                   Sair
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                            @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Scripts -->
<script type="text/javascript">

    jQuery.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });

    $(document).ready(function () {
        $('.tb_body').on('click','.btn_exclude',function () {
           console.log("teste");
            jQuery.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });
            idPlayer = $(this).data('iddelete');
            $.ajax({

                type:'post',

                url:'/ajaxRequestRemovePlayer',

                data:{'idPlayer': idPlayer, 'idTeam': $("#add_plpayer").data('idteam')},

                success:function(data){
                    data =  JSON.parse(data);
                    $('.tb_body').html(data.playerTable);
                    $('#id_jogador').html(data.player);
                    console.log(data);

                }

            });
        });
        $("#add_plpayer").on('click', function () {
            jQuery.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });
            idPlayer = $('#id_jogador option:selected').val();
            $.ajax({

                type:'post',

                url:'/ajaxRequestAddPlayer',

                data:{'idPlayer': idPlayer, 'idTeam': $(this).data('idteam')},

                success:function(data){
                    data =  JSON.parse(data);
                    $('.tb_body').html(data.playerTable);
                    $('#id_jogador').html(data.player);
                    console.log(data);

                }

            });

        });

    $("#id_estado").on('change',function(e){

        jQuery.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });

        $.ajax({

            type:'GET',

            url:'/ajaxRequest/'+$(this).val(),

            data:{},

            success:function(data){
                $('#id_city').html(data);
                console.log(data);

            }

        });



    })


        $(".exportCsvCommand").on('click',function(e){
            jQuery.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });
            swal("Porfavor informe o email.", {
                content: "input",
            })
                .then((value) => {
                console.log(value);
                if(value != null && value != '') {
                    $.ajax({

                        type:'GET',

                        url:'/socios/exportCommand/'+value,

                        data:{},

                        success:(data) => {
                            swal({
                                title: "Solicitação feita com sucesso!",
                                text: "Em breve você recebera o arquivo no email "+value+ '.',
                                icon: "success",
                            });

                        }

                    });
                }


            });






    })



    });

</script>
@yield('js')
</body>
</html>