@extends('layouts.default')
@section('css')
    <style>
        .form-group.required label:after {
            content: " *";
            color: red;
            font-weight: bold;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
            <div class="card uper">
                <div class="card-header">
                    Adicionar Socio
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form method="post" action="{{isset($model) ? route('support.atualizar',$model->id) : route('support.create')}}">
                        <div class="form-group">
                            @csrf
                            <label for="name">Nome</label>
                            <input type="text" class="form-control" name="name" value="{{isset($model) ? $model->name : ''}}"/>
                        </div>
                        <div class="form-group">
                            <label for="name">Data de nascimento</label>
                            <input type="date" class="form-control" name="date_of_birth" value="{{isset($model ) ? $model->date_of_birth : ''}}"/>

                        </div>
                        <div class="form-group">
                            <label for="name">CPF</label>
                            <input type="text" class="form-control" name="cpf" value="{{isset($model) ? $model->cpf : ''}}" />
                        </div>

                        <div class="form-group">
                            <label for="price">Estado:</label>
                            <select  class="form-control " name="id_estado" id="id_estado">
                                <option value="">Selecione</option>
                                @foreach($states  as $state)
                                     <option {{ isset($model) && $model->city->state_id == $state->id ? 'selected':''}} value="{{$state->id}}">{{$state->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price">Cidade:</label>
                            <select  class="form-control " name="id_city" id="id_city">
                                <option value=""></option>
                                @if(isset($cities))
                                    @foreach($cities  as $city)
                                        <option {{isset($model) && $model->id_city == $city->id ? 'selected':''}} value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach
                               @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price">Time:</label>
                            <select  class="form-control " name="id_team" id="id_team">
                                <option value=""></option>
                                @if(isset($teams))
                                    @foreach($teams  as $team)
                                        <option {{isset($model) && $model->id_team == $team->id ? 'selected':''}} value="{{$team->id}}">{{$team->name}}</option>
                                    @endforeach
                               @endif
                            </select>
                        </div>



                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </form>


                </div>
            </div>
        </div>


    </div>

@endsection