
## Desafio Vhsys

Aplpicação desenvolvida como desafio para vaga de desenvolvedor na Vhsys

**Desafio 1**

Preciso que seja desenvolvido um aplicativo em PHP com Mysql, onde eu possa fazer o controle de times de futebol.
Para isso é necessario que eu possa efetuar:
- Cadastro de Times de Futebol: Deve conter os dados basicos cadastrais de um time de futebol
- Cadastro de Jogadores de futebol: Deve conter os dados basicos cadastrais de jogador de futebol
- Vinculo de Times X Jogadores
- Consultar tanto times como seus jogadores
- Poder exportar os dados da consulta em CSV ou Excel.

**Desafio 2**

Temos um "problema" que é o seguinte: Cliente precisa fazer download de muitos XML, porém quando esse volume é muito grande, ou seja, passa de 5 minutos fazendo o download na pagina, ocorre Timeout na pagina HTML. Gostaria que você montasse uma arquitetura para download desses arquivos que aconteça de forma background e o usuario seja notificado quando o download terminar, sem a necessidade de ficar aguardando.
- Você tem toda a liberdade de fazer em qualquer linguagem / estrutura / etc.
- Esse desafio vc tem carta branca.
- Queremos que você nos supreenda neste desafio.


## Dependências

A API foi desenvolvid com LARAVEL 5.8, então  será necessário instalar algum servidor PHP, aconselho o pacote do WAMP que ja te fornece tudo que é necessário para rodar a api.

	- PHP 7.* >
	- MySQL

## Acesso
	- Login: admin@admin.com
	- Password: admin

## Como utilizar

1° Faça o clone do projeto utilizando o seguinte comando.

	- git clone git@bitbucket.org:lukSimoes/desafiovhsys.git

2° Rode o composer para instalar as dependências

	- Compopser update

3° Crie um Banco de dados mysq, descomente o arquivo .env.example deixando apenas .env e coloque os dados de conexão.

	- DB_CONNECTION=mysql
	  DB_HOST=localhost
	  DB_PORT=3306
	  DB_DATABASE= minhaBaseDeDados
	  DB_USERNAME=root
	  DB_PASSWORD=

4° Gere a API KEY da sua aplicação

	- php artisan key:generate 

5° Como um dos desafios era fazer uma solução para rodar em background para não prender o usuario enquanto exportamos os dados
utilizei a Queue do laravel para isso, então será necessário fazer uma configuração para utilizar, no arquivo .env altere ou adicione a seguinte linha .

    - QUEUE_CONNECTION=database
Após isso precisamos configurar a autenticação do email para que possamos enviar o link com o arquivo gerado, ainda no .env

        - MAIL_DRIVER=smtp
          MAIL_HOST=smtp.gmail.com
          MAIL_PORT=587
          MAIL_USERNAME=UmEmailDaGmail
          MAIL_PASSWORD=Minhasenha
          MAIL_ENCRYPTION=tls
          
O gmail é preciso permitir que um aplicativo envie email com sua conta acesse o link abaixo e clique em permitir.

    https://myaccount.google.com/lesssecureapps?pli=1
    
6° Gere o link de espelho para permitir acesso aos arquivos gerados.

    php artisan storage:link


7° Como ultimo passo rode as migrations e os seeds

	-  php artisan migrate
	-  php artisan db:seed -v

8° Como estamos utilizando Queue é necessario rodar um comando para ele ficar escutando se tem algo para ser executado rode o seguinte comando.

     php artisan queue:work
     
6° Pronto a aplicação está configurada.