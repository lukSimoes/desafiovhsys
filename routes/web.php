<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/times', 'TeamController@index')->name('form.index');
Route::get('/times/novo', 'TeamController@create')->name('form.new');
Route::get('/times/editar/{id}', 'TeamController@edit')->name('form.editar');
Route::get('/times/deletar/{id}', 'TeamController@delete')->name('form.deletar');
Route::post('/times/atualizar/{id}', 'TeamController@update')->name('form.atualizar');
Route::post('/times/salvar', 'TeamController@create')->name('form.create');
Route::get('/ajaxRequest/{id}', 'TeamController@ajaxRequest');
Route::post('/ajaxRequestAddPlayer', 'TeamController@ajaxRequestAddPlayer');
Route::post('/ajaxRequestRemovePlayer', 'TeamController@ajaxRequestRemovePlayer');
Route::get('/times/export', 'TeamController@downloadExcel')->name('form.export');



Route::get('/jogadores', 'PlayerController@index')->name('player.index');
Route::get('/jogadores/novo', 'PlayerController@create')->name('player.new');
Route::get('/jogadores/editar/{id}', 'PlayerController@edit')->name('player.editar');
Route::get('/jogadores/deletar/{id}', 'PlayerController@delete')->name('player.deletar');
Route::post('/jogadores/atualizar/{id}', 'PlayerController@update')->name('player.atualizar');
Route::post('/jogadores/salvar', 'PlayerController@create')->name('player.create');
Route::get('/jogadores/export', 'PlayerController@downloadExcel')->name('player.export');
Route::get('/jogadores/exportCommand', 'PlayerController@exportCommand')->name('player.exportCommand');


Route::get('/socios', 'SupporterController@index')->name('support.index');
Route::get('/socios/novo', 'SupporterController@create')->name('support.new');
Route::get('/socios/editar/{id}', 'SupporterController@edit')->name('support.editar');
Route::get('/socios/deletar/{id}', 'SupporterController@delete')->name('support.deletar');
Route::post('/socios/atualizar/{id}', 'SupporterController@update')->name('support.atualizar');
Route::post('/socios/salvar', 'SupporterController@create')->name('support.create');
Route::get('/socios/export', 'SupporterController@downloadExcel')->name('support.export');
Route::get('/socios/exportCommand/{email}', 'SupporterController@exportCommand')->name('support.exportCommand');
