<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i = 1; $i <= 3; $i++){
            $faker = Faker::create('App\Team');
            $teamInsert = new \App\Team();
            $teamInsert->name = $faker->sentence();
            $teamInsert->id_city = 2927408;
            $teamInsert->mascot =  $faker->sentence();
            $teamInsert->founded_in = $faker->date('Y-m-d');
            $teamInsert->save();
            for($b = 1; $b<=2000; $b++) {
                DB::table('supporters')->insert([
                    'name' => $faker->name(),
                    'date_of_birth' =>$faker->date('Y-m-d'),
                    'cpf' =>'054.754.965-91',
                    'id_team' =>$teamInsert->id,
                    'id_city' =>2927408,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]);
            }
        }

    }
}
