<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',100);
            $table->string('height');
            $table->string('weight');

            $table->integer('id_city')->unsigned();

            $table->integer('id_team')->nullable();

            $table->date('date_of_birth');
            $table->timestamps();
        });

        Schema::table('players', function($table) {
            $table->foreign('id_city')->references('id')->on('cities');
            $table->foreign('id_team')->references('id')->on('teams')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
